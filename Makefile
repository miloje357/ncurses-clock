P=todo
OBJECTS=
CFLAGS = -g -Wall -O3
LDLIBS= -lncurses -lm
CC=gcc

all:
	$(CC) $(CFLAGS) $(LDLIBS) -o Clock ncurses_clock.c
