#include <curses.h>
#include <ncurses.h>
#include <time.h>
#include <locale.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define RESOLUTION 75
#define HEIGHT 5
#define LENGTH 5
#define DENSITY 0.01

typedef struct {
    bool vis;
    int y; int x;
} star;

double getdiff(void);
bool should_update(time_t t, double diff);
void update_time(char timestr[]);
void display_time(char timestr[]);
void display_num(int y, int x, char c);
void rectangle(int y1, int x1, int y2, int x2);
void getstars();
void display_star();
void remove_star();
void animate_star();
bool randchoose();

static const char *numchars[11][HEIGHT] = {
        {"████", "█  █", "█  █", "█  █", "████"},
        {"█   ", "█   ", "█   ", "█   ", "█   "},
        {"████", "   █", "████", "█   ", "████"},
        {"████", "   █", "████", "   █", "████"},
        {"█  █", "█  █", "████", "   █", "   █"},
        {"████", "█   ", "████", "   █", "████"},
        {"████", "█   ", "████", "█  █", "████"},
        {"████", "   █", "   █", "   █", "   █"},
        {"████", "█  █", "████", "█  █", "████"},
        {"████", "█  █", "████", "   █", "████"},
        {"  ", "  ", "█ ", "  ", "█ "}
    };

static int numstars = 0;
static star *stars;
static int maxstars = 0;

double getdiff(void)
{
    return 60 - time(NULL) % 60;
}

bool should_update(time_t t, double diff)
{
    return diff - difftime(time(NULL), t) < RESOLUTION / 1000.0;
}

void update_time(char timestr[])
{
    time_t rawtime;
    struct tm * timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(timestr, 10, "%R", timeinfo);
}

void display_time(char timestr[])
{
    int y, x;
    getmaxyx(stdscr, y, x);
    y = y/2 - (HEIGHT + 2) / 2; x = x/2 - (LENGTH*5)/2;
    rectangle(y, x, y + HEIGHT + 1, x + LENGTH*5);
    y++; x++;
    for (int i = 0; i < LENGTH; i++) {
        display_num(y, x, timestr[i]);
        x += 5;
    }
}

void rectangle(int y1, int x1, int y2, int x2)
{
    mvhline(y1, x1, 0, x2-x1);
    mvhline(y2, x1, 0, x2-x1);
    mvvline(y1, x1, 0, y2-y1);
    mvvline(y1, x2, 0, y2-y1);
    mvaddch(y1, x1, ACS_ULCORNER);
    mvaddch(y2, x1, ACS_LLCORNER);
    mvaddch(y1, x2, ACS_URCORNER);
    mvaddch(y2, x2, ACS_LRCORNER);
}

void display_num(int y, int x, char c)
{
    for (int i = 0; i < 5; i++)
        mvaddstr(y + i, x, numchars[c - '0'][i]);
}

void getstars()
{
    int maxy, maxx;
    getmaxyx(stdscr, maxy, maxx);
    int area = maxy * maxx;
    area -= (2 + HEIGHT) * (2 + 5 * LENGTH);
    maxstars = area * DENSITY;
    stars = malloc(sizeof(star) * maxstars);
}

void display_star()
{
    int maxx, maxy;
    int recty, rectx;
    getmaxyx(stdscr, maxy, maxx);
    recty = maxy/2 - (HEIGHT + 2) / 2; rectx = maxx/2 - (LENGTH * 5) / 2;
    int y = rand() % (maxy + 1), x = rand() % (maxx + 1);
    while (recty <= y && y <= recty + HEIGHT + 1 && rectx <= x && x <= rectx + LENGTH * 5) {
        y = rand() % (maxy + 1), x = rand() % (maxx + 1);
    }
    for (int i = 0; i < maxstars; i++)
        if (!stars[i].vis) {
            stars[i].y = y; stars[i].x = x;
            stars[i].vis = true;
            mvaddch(y, x, '*');
            break;
        }
}

void remove_star()
{
    int i = rand() % numstars;
    mvaddch(stars[i].y, stars[i].x, ' ');
    stars[i].vis = false;
}

bool randchoose()
{
    return rand() % 2 == 1;
}

void animate_star()
{
    if (numstars < 3) {
        display_star();
        numstars++;
    } else if (numstars > maxstars) {
        remove_star();
        numstars--;
    } else if (randchoose()) {
        display_star();
        numstars++;
    } else {
        remove_star();
        numstars--;
    }
}

int main()
{
    char timestr[LENGTH];
    double diff = getdiff();
    time_t t = time(NULL);
    int h, w;
    getmaxyx(stdscr, h, w);
    setlocale(LC_CTYPE, "");
    initscr();
    noecho();
    curs_set(0);
    srand(time(NULL));
    getstars();
    update_time(timestr);
    display_time(timestr);
    for (;;) {
        int temp_h, temp_w;
        getmaxyx(stdscr, temp_h, temp_w);
        if (temp_h != h || temp_w != w) {
            clear();
            display_time(timestr);
            h = temp_h;
            w = temp_w;
        }
        if (should_update(t, diff)) {
            update_time(timestr);
            display_time(timestr);
            diff = 60;
            t = time(NULL);
        }
        animate_star();
        refresh();
        usleep(RESOLUTION*1000);
    }
    endwin();
    return 0;
}
